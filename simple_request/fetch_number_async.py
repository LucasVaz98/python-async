import asyncio

async def fetch_data():
    print('Start Fetching')
    await asyncio.sleep(2)
    print('Done Fetching')
    return {'data': 1}

async def print_numbers():
    for i in range(20):
        print(i)
        await asyncio.sleep(0.25)

async def main():
    task1 = asyncio.create_task(fetch_data())
    task2 = asyncio.create_task(print_numbers())

    value_1 = await task1
    await task2

asyncio.run(main())

