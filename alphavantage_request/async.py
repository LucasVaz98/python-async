import asyncio
import aiohttp
import os
from dotenv import load_dotenv
import time

load_dotenv()
_api_key = os.getenv('api_key')

symbols = ['AAPL', 'AAPL',  'GOOG', 'TSLA', 'MSFT',  'GOOG', 'TSLA', 'MSFT',  'GOOG', 'TSLA', 'MSFT',  'GOOG', 'TSLA', 'MSFT',  'GOOG', 'TSLA', 'MSFT',  'GOOG', 'TSLA', 'MSFT',  'GOOG', 'TSLA', 'MSFT',  'GOOG', 'TSLA', 'MSFT',  'GOOG', 'TSLA', 'MSFT',  'GOOG', 'TSLA', 'MSFT',  'GOOG', 'TSLA', 'MSFT',  'GOOG', 'TSLA', 'MSFT',  'GOOG', 'TSLA', 'MSFT',  'GOOG', 'TSLA', 'MSFT',  'GOOG', 'TSLA', 'MSFT',  'GOOG', 'TSLA', 'MSFT',  'GOOG', 'TSLA', 'MSFT',  'GOOG', 'TSLA', 'MSFT',  'GOOG', 'TSLA', 'MSFT',  'GOOG', 'TSLA', 'MSFT',  'GOOG', 'TSLA', 'MSFT',  'GOOG', 'TSLA', 'MSFT',  'GOOG', 'TSLA', 'MSFT',  'GOOG', 'TSLA', 'MSFT',  'GOOG', 'TSLA', 'MSFT',  'GOOG', 'TSLA', 'MSFT',  'GOOG', 'TSLA', 'MSFT',  'GOOG', 'TSLA', 'MSFT',  'GOOG', 'TSLA', 'MSFT',  'GOOG', 'TSLA', 'MSFT',  'GOOG', 'TSLA', 'MSFT',  'GOOG', 'TSLA', 'MSFT',  'GOOG', 'TSLA', 'MSFT',  'GOOG', 'TSLA', 'MSFT',  'GOOG', 'TSLA', 'MSFT',  'GOOG', 'TSLA', 'MSFT',  'GOOG', 'TSLA', 'MSFT',  'GOOG', 'TSLA', 'MSFT',  'GOOG', 'TSLA', 'MSFT',  'GOOG', 'TSLA', 'MSFT',  'GOOG', 'TSLA', 'MSFT',  'GOOG', 'TSLA', 'MSFT', 'GOOG', 'TSLA', 'MSFT', 'GOOG', 'TSLA', 'MSFT']
results = []

def get_tasks(session):
    tasks = []
    count_requests = 0
    for symbol in symbols:
        url = f'https://www.alphavantage.co/query?function=OVERVIEW&symbol={symbol}&apikey={_api_key}'
        tasks.append(asyncio.create_task(session.get(url=url, ssl=False)))
        count_requests += 1

    print(f"Requested {count_requests} requests")

    return tasks


async def get_symbols():
    start = time.time()

    async with aiohttp.ClientSession() as session:
        tasks = get_tasks(session)
        responses = await asyncio.gather(*tasks)

        for response in responses:
            results.append(await response.json())

    end = time.time()

    print(f'We did it! It took {end - start}s')


asyncio.set_event_loop_policy(asyncio.WindowsSelectorEventLoopPolicy())
asyncio.run(get_symbols())
