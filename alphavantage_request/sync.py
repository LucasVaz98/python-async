import requests
import os
from dotenv import load_dotenv
import time

load_dotenv()

_api_key = os.getenv('api_key')

print(_api_key)

symbols = ['AAPL', 'GOOG', 'TSLA', 'MSFT']
results = []

start = time.time()
count_requests = 0
for symbol in symbols:
    print(f'working on symbol {symbol}!')
    url = f'https://www.alphavantage.co/query?function=OVERVIEW&symbol={symbol}&apikey={_api_key}'
    response = requests.get(url=url)
    results.append(response.json())
    count_requests += 1

end = time.time()

for result in results:
    print(result)


print(f'We did it! But it took {end - start}s to process {count_requests} itens.')
