import asyncio
import aiohttp
import time
import os
import motor.motor_asyncio
from dotenv import load_dotenv

symbols = ["AAPL", "GOOG", "TSLA", "MSFT"]

async def get_tasks(session, urls):
    tasks = []
    count_requests = 0

    for url in urls:
        tasks.append(asyncio.create_task(session.get(url=url, ssl=False)))
        count_requests += 1

    print(f"Fetched {count_requests} itens")

    return tasks


async def get_stocks_info(urls):
    start_api = time.time()
    results = []

    async with aiohttp.ClientSession() as session:
        tasks = await get_tasks(session, urls)
        responses = await asyncio.gather(*tasks)

        for response in responses:
            results.append(await response.json())

    print(f"Did it in {time.time():.3}s")

    return results


def build_urls():
    urls = []
    _api_key = os.getenv("api_key")
    _url_base = os.getenv("url_base")

    for item in symbols:
        urls.append(f"{_url_base}&symbol={item}&apikey={_api_key}")
    print(urls)
    return urls


async def get_server_info(conn_str):
    print(conn_str)
    client = motor.motor_asyncio.AsyncIOMotorClient(conn_str, serverSelectionTimeoutMS=5000)

    try:
        print(await client.server_info())
        return client
    except Exception:
        print("Unable to connect to the server")


async def load(client, stocks):
    db = client['stocks']
    collection_stocks = db['stocks_data']

    for stock in stocks:
        old_document = await collection_stocks.find_one({'Symbol': stock["Symbol"]})

        if old_document is not None and '_id' in old_document.keys():
            result = await collection_stocks.replace_one({'_id': old_document['_id']}, stock)
        else:
            result = await collection_stocks.insert_one(stock)
        
        print(result)


async def main():
    _conn_string = os.getenv("mongo_conn_str")    

    urls=build_urls()
    stocks = await get_stocks_info(urls)

    for stock in stocks:
        print(stocks)

    client = await get_server_info(_conn_string)

    if type(client) is not type('str'):
        await load(client, stocks)

if __name__ == "__main__":
    load_dotenv()
    asyncio.set_event_loop_policy(asyncio.WindowsSelectorEventLoopPolicy())
    asyncio.run(main())

